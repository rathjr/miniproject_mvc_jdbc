package SRG3.Model.DTO;

import java.time.LocalDate;

public class StockDTO {
    private int id,qty;
    private double unitprice;
    private String name;

    public StockDTO(){}
    public StockDTO(int id, String name, double unitprice,int qty ) {
        this.id = id;
        this.qty = qty;
        this.unitprice = unitprice;
        this.name = name;
    }
    LocalDate myObj = LocalDate.now();
    public LocalDate getMyObj() {
        return myObj;
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public double getUnitprice() {
        return unitprice;
    }

    public void setUnitprice(double unitprice) {
        this.unitprice = unitprice;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
