package SRG3.Model.DAO;

import SRG3.Model.DTO.StockDTO;
import SRG3.View.StockView;
import org.nocrala.tools.texttablefmt.BorderStyle;
import org.nocrala.tools.texttablefmt.CellStyle;
import org.nocrala.tools.texttablefmt.ShownBorders;
import org.nocrala.tools.texttablefmt.Table;

import javax.swing.plaf.IconUIResource;
import java.awt.*;
import java.sql.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

public class StockDAO {

    private ArrayList<StockDTO> object;
    private ArrayList<String> tablename;
    String tbname="stock";
    //  default constructor read data from Database
    public StockDAO(){
        object=new ArrayList<>();
        tablename=new ArrayList<>();
        Connection connection=getConnection();
        SelectData(connection);
    }
    LocalDateTime myDateObj = LocalDateTime.now();
    DateTimeFormatter myFormatObj = DateTimeFormatter.ofPattern("dd-MM-yyyy-SS");
    String formattedDate = myDateObj.format(myFormatObj);
    String[] arrOfStr = formattedDate.split("-");
    int a=Integer.parseInt(arrOfStr[0]+arrOfStr[1]+arrOfStr[2]+ arrOfStr[3]);
    public Connection getConnection(){
        Connection connection=null;
        try {
//        load driver
            Class.forName("org.postgresql.Driver");
//        Connect to Database
            connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/DB2", "postgres", "Rathkhmer2021");
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
        return connection ;
    }

    //  Read record from database and write to List
    public void SelectData(Connection connection){
        try {
//        create Statment (have 3type)
            String sl="Select * from "+tbname+"";
            PreparedStatement ps=connection.prepareStatement(sl);
//            execute Query (have 2type)
            ResultSet resultSet= ps.executeQuery();
            while (resultSet.next()){
                StockDTO stockDTO=new StockDTO();
                stockDTO.setId(resultSet.getInt("id"));
                stockDTO.setName(resultSet.getString("name"));
                stockDTO.setUnitprice(resultSet.getDouble("unitprice"));
                stockDTO.setQty(resultSet.getInt("qty"));
                object.add(stockDTO);
            }
            ps.close();
            resultSet.close();
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }

    //    Write List data to Database
    public void Savedata(Connection connection){
        int rowinsert=1,count=0;
        PreparedStatement ps=null;
        try{
            String del="Delete from stock";
            String its="INSERT INTO stock(id,name,unitprice,qty) VALUES (?,?,?,?)";
            ps=connection.prepareStatement(del);
            ps.executeUpdate();
            ps=connection.prepareStatement(its);
            for(StockDTO tmp:object) {
                ps.setInt(1, tmp.getId());
                ps.setString(2, tmp.getName());
                ps.setDouble(3, tmp.getUnitprice());
                ps.setInt(4, tmp.getQty());
                ps.addBatch();
                count++;
                if(count==object.size()){
                    ps.executeBatch();
                }
            }
            if(rowinsert<1)
                System.out.println("=> Insert To Database Failed !");
            else
                System.out.println("=> Insert To Database Successfully");
            ps.close();
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
    }
    //  Back up table
    public void Backup(Connection connection){
        PreparedStatement ps = null;
        try {
            String sl = "Select * INTO Backup"+a+" from stock";
//            execute Query (have 2type)
            ps = connection.prepareStatement(sl);
            ps.executeUpdate();
            ps.close();
            System.out.println("=> Table Backup"+a+" was backup successfully...");
//            count++;
        }catch (Exception e){
//            count++;
            System.out.println(e.getMessage());
        }
    }
    //  restore data
    public void Restore(Connection connection){
        try {
//        create Statment (have 3type)
            String sl="SELECT table_name as Tbname FROM information_schema.tables WHERE table_schema='public'";
            PreparedStatement ps=connection.prepareStatement(sl);
//            execute Query (have 2type)
            ResultSet resultSet= ps.executeQuery();
            while (resultSet.next()){
                tablename.add(resultSet.getString("Tbname"));
            }
            System.out.println(a);
            ps.close();
            resultSet.close();
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
    //    Write List data to Database
    public void BackupData(Connection connection){
        System.out.println("Hello Moni");
    }

    public void Writedata(StockDTO stockDTO){
        StockView stockView=new StockView();
        stockView.verifyInsertshort(stockDTO);
        stockView.verifyInsert(stockDTO,object);
    }

    public void Writeshorthand(StockDTO stockDTO){
        StockView stockView=new StockView();
        stockView.verifyInsertshort(stockDTO);
        stockView.verifyInsert(stockDTO,object);
    }
    //  get all object Stock of Arraylist
    public List Display(){
        return object;
    }
    public List Displaytbn(){
        return tablename;
    }

    //    Readid from UI and search in list
    public void SearchbyID(int id){
        int count=0;
        for(StockDTO st:object){
            if(id==st.getId()){
                CellStyle numberStyle1 = new CellStyle(CellStyle.HorizontalAlign.left);
                Table t1 = new Table(1, BorderStyle.UNICODE_DOUBLE_BOX, ShownBorders.SURROUND);
                t1.setColumnWidth( 0,32,32);
                t1.addCell(" ID :"+st.getId(),numberStyle1);
                t1.addCell( " Name :"+st.getName(), numberStyle1);
                t1.addCell(" UnitPrice :"+st.getUnitprice(),numberStyle1);
                t1.addCell(" Qty :"+st.getQty(),numberStyle1);
                t1.addCell(" Date :"+st.getMyObj(),numberStyle1);
                System.out.println(t1.render());
                count++;
            }
        }
        if(count==0){
            System.out.println("-> Your ID id EXIST....");
        }
    }
    //    Readid from UI and Delete in list
    public Iterator DeletebyID(int id){
        int veri,count=0;
        Iterator<StockDTO> it = object.iterator();
        while (it.hasNext()) {
            StockDTO tmp = it.next();
            if (id == tmp.getId()) {
                CellStyle numberStyle1 = new CellStyle(CellStyle.HorizontalAlign.left);
                Table t1 = new Table(1, BorderStyle.UNICODE_DOUBLE_BOX, ShownBorders.SURROUND);
                t1.setColumnWidth( 0,32,32);
                t1.addCell(" ID :"+tmp.getId(),numberStyle1);
                t1.addCell( " Name :"+tmp.getName(), numberStyle1);
                t1.addCell(" UnitPrice :"+tmp.getUnitprice(),numberStyle1);
                t1.addCell(" Qty :"+tmp.getQty(),numberStyle1);
                t1.addCell(" Date :"+tmp.getMyObj(),numberStyle1);
                System.out.println(t1.render());
                StockView stockView=new StockView();
                stockView.verifyDelete(it);
                count++;
            }
        }
        if (count == 0)
            System.out.println("=> ID : " + id + " IS NOT EXIST...");
        return it;
    }

    //    Read Name from UI and Search in list
    public void SearchbyName(String search){
        int count=0;
        CharSequence seq = search;
        boolean bool;
        for(StockDTO st: object){
            bool=st.getName().contains(seq);
            if(search.equals(st.getName())|| bool==true){
                CellStyle numberStyle1 = new CellStyle(CellStyle.HorizontalAlign.left);
                Table t1 = new Table(1, BorderStyle.UNICODE_DOUBLE_BOX, ShownBorders.SURROUND);
                t1.setColumnWidth( 0,32,32);

                t1.addCell(" ID :"+st.getId(),numberStyle1);
                t1.addCell( " Name :"+st.getName(), numberStyle1);
                t1.addCell(" UnitPrice :"+st.getUnitprice(),numberStyle1);
                t1.addCell(" Qty :"+st.getQty(),numberStyle1);
                t1.addCell(" Date :"+st.getMyObj(),numberStyle1);

                System.out.println(t1.render());
                count++;
            }
        }
        if(count==0){
            System.out.println("=> search not found");
        }
    }

    //    Read id from UI and search in list
    public void Searchidupdate(int id) {
        int count = 0;
        for (StockDTO st : object) {
            if (id == st.getId()) {
                CellStyle numberStyle1 = new CellStyle(CellStyle.HorizontalAlign.left);
                Table t1 = new Table(1, BorderStyle.UNICODE_DOUBLE_BOX, ShownBorders.SURROUND);
                t1.setColumnWidth(0, 32, 32);

                t1.addCell(" ID :" + st.getId(), numberStyle1);
                t1.addCell(" Name :" + st.getName(), numberStyle1);
                t1.addCell(" UnitPrice :" + st.getUnitprice(), numberStyle1);
                t1.addCell(" Qty :" + st.getQty(), numberStyle1);
                t1.addCell(" Date :" + st.getMyObj(), numberStyle1);
                System.out.println(t1.render());
                StockView stockView=new StockView();
                stockView.menuUpdate(id,object);
                count++;
            }
        }
        if(count==0){
            System.out.println("-> Your ID id EXIST....");
        }
    }


    //    Pagination section
    public void FirstPage(){
        Statement statement = null;
        try {

            getConnection().setAutoCommit(false);
            statement = getConnection().createStatement();

            //2- Select all data from tbl_setrow to display current row
            ResultSet resultSet = statement.executeQuery( "SELECT * FROM tbl_setrow ORDER BY id DESC \n" +
                    "LIMIT 1" );
            while ( resultSet.next() ) {
                int chunk = resultSet.getInt("row");
                int totalPage;
                if (object.size() % chunk != 0) {
                    totalPage = (object.size() / chunk) + 1;
                } else {
                    totalPage = (object.size() / chunk);
                }
                int length;
                int page =1;
                int tmpRow = (page * chunk) - chunk;
                if (page != totalPage) {
                    length = page * chunk;
                } else {
                    length = object.size();
                }

                CellStyle numberStyle = new CellStyle(CellStyle.HorizontalAlign.center);
                Table t = new Table(5, BorderStyle.UNICODE_DOUBLE_BOX,
                        ShownBorders.ALL);

                t.setColumnWidth(0, 8, 16);
                t.setColumnWidth(1, 16, 32);
                t.setColumnWidth(2, 16, 32);
                t.setColumnWidth(3, 16, 32);
                t.setColumnWidth(4, 16, 32);

                t.addCell("ID", numberStyle);
                t.addCell("Name", numberStyle);
                t.addCell("Unit Price", numberStyle);
                t.addCell("Qty", numberStyle);
                t.addCell("Imported Date", numberStyle);
                for (int row = tmpRow; row < length; row++) {

                    t.addCell("" + object.get(row).getId(), numberStyle);
                    t.addCell("" + object.get(row).getName(), numberStyle);
                    t.addCell("" + object.get(row).getUnitprice(), numberStyle);
                    t.addCell("" + object.get(row).getQty(), numberStyle);
                    t.addCell("" + object.get(row).getMyObj(), numberStyle);

                }
                Table t1 = new Table(2, BorderStyle.CLASSIC_LIGHT, ShownBorders.SURROUND);
                CellStyle right = new CellStyle(CellStyle.HorizontalAlign.right);
                CellStyle left = new CellStyle(CellStyle.HorizontalAlign.left);
                t1.setColumnWidth(0, 60, 100);
                t1.addCell(" Page: " + page + "/" + totalPage,left);
                t1.addCell("Total records: " + object.size()+" ",right);
                System.out.println(t.render());
                System.out.println(t1.render());

            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
    //      Previous Page
    public void previousPage(int p){

        Statement statement = null;
        try {

            getConnection().setAutoCommit(false);
            statement = getConnection().createStatement();

            //2- Select all data from tbl_setrow to display current row
            ResultSet resultSet = statement.executeQuery( "SELECT * FROM tbl_setrow ORDER BY id DESC \n" +
                    "LIMIT 1" );
            while ( resultSet.next() ) {
                int chunk = resultSet.getInt("row");
                int totalPage;
                int page = p;
                if (object.size() % chunk != 0) {
                    totalPage = (object.size() / chunk) + 1;
                } else {
                    totalPage = (object.size() / chunk);
                }
                CellStyle numberStyle = new CellStyle(CellStyle.HorizontalAlign.center);
                Table t = new Table(5, BorderStyle.UNICODE_DOUBLE_BOX,
                        ShownBorders.ALL);

                t.setColumnWidth(0, 8, 16);
                t.setColumnWidth(1, 16, 32);
                t.setColumnWidth(2, 16, 32);
                t.setColumnWidth(3, 16, 32);
                t.setColumnWidth(4, 16, 32);

                t.addCell("ID", numberStyle);
                t.addCell("Name", numberStyle);
                t.addCell("Unit Price", numberStyle);
                t.addCell("Qty", numberStyle);
                t.addCell("Imported Date", numberStyle);

                //print title
                if (object.size() > page * chunk) {
                    for (int i = (page * chunk - chunk);
                         i < page * chunk; i++) {
                        t.addCell("" + object.get(i).getId(), numberStyle);
                        t.addCell("" + object.get(i).getName(), numberStyle);
                        t.addCell("" + object.get(i).getUnitprice(), numberStyle);
                        t.addCell("" + object.get(i).getQty(), numberStyle);
                        t.addCell("" + object.get(i).getMyObj(), numberStyle);
                    }
                } else {
                    for (int i = (page * chunk - chunk); i < object.size(); i++) {
                        t.addCell("" + object.get(i).getId(), numberStyle);
                        t.addCell("" + object.get(i).getName(), numberStyle);
                        t.addCell("" + object.get(i).getUnitprice(), numberStyle);
                        t.addCell("" + object.get(i).getQty(), numberStyle);
                        t.addCell("" + object.get(i).getMyObj(), numberStyle);
                    }
                }

                Table t1 = new Table(2, BorderStyle.CLASSIC_LIGHT, ShownBorders.SURROUND);
                CellStyle right = new CellStyle(CellStyle.HorizontalAlign.right);
                CellStyle left = new CellStyle(CellStyle.HorizontalAlign.left);
                t1.setColumnWidth(0, 60, 100);
                t1.addCell(" Page: " + page + "/" + totalPage,left);
                t1.addCell("Total records: " + object.size()+" ",right);
                System.out.println(t.render());
                System.out.println(t1.render());
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

    }
    //      next Page
    public void nextPage(int p){
        Statement statement = null;
        try {

            getConnection().setAutoCommit(false);
            statement = getConnection().createStatement();

            //2- Select all data from tbl_setrow to display current row
            ResultSet resultSet = statement.executeQuery( "SELECT * FROM tbl_setrow ORDER BY id DESC \n" +
                    "LIMIT 1" );
            while ( resultSet.next() ) {
                int chunk = resultSet.getInt("row");
                int totalPage;
                if (object.size() % chunk != 0) {
                    totalPage = (object.size() / chunk) + 1;
                } else {
                    totalPage = (object.size() / chunk);
                }
                CellStyle numberStyle = new CellStyle(CellStyle.HorizontalAlign.center);
                Table t = new Table(5, BorderStyle.UNICODE_DOUBLE_BOX,
                        ShownBorders.ALL);

                t.setColumnWidth(0, 8, 16);
                t.setColumnWidth(1, 16, 32);
                t.setColumnWidth(2, 16, 32);
                t.setColumnWidth(3, 16, 32);
                t.setColumnWidth(4, 16, 32);

                t.addCell("ID", numberStyle);
                t.addCell("Name", numberStyle);
                t.addCell("Unit Price", numberStyle);
                t.addCell("Qty", numberStyle);
                t.addCell("Imported Date", numberStyle);

                //print title
                if (object.size() > p * chunk) {
                    for (int i = (p * chunk - chunk);
                         i < p * chunk; i++) {
                        t.addCell("" + object.get(i).getId(), numberStyle);
                        t.addCell("" + object.get(i).getName(), numberStyle);
                        t.addCell("" + object.get(i).getUnitprice(), numberStyle);
                        t.addCell("" + object.get(i).getQty(), numberStyle);
                        t.addCell("" + object.get(i).getMyObj(), numberStyle);
                    }
                }

                Table t1 = new Table(2, BorderStyle.CLASSIC_LIGHT, ShownBorders.SURROUND);
                CellStyle right = new CellStyle(CellStyle.HorizontalAlign.right);
                CellStyle left = new CellStyle(CellStyle.HorizontalAlign.left);
                t1.setColumnWidth(0, 60, 100);
                t1.addCell(" Page: " + p + "/" + totalPage,left);
                t1.addCell("Total records: " + object.size()+" ",right);
                System.out.println(t.render());
                System.out.println(t1.render());
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
    //      Last page
    public void lastPage(){
        int chunk = 3;
        int totalPage;
        int page = 1;
        if (object.size() % chunk != 0) {
            totalPage = (object.size() / chunk) + 1;
        } else {
            totalPage = (object.size() / chunk);
        }
        int length;
        page = totalPage;
        int tmpRow = (page * chunk) - chunk;
        if (page != totalPage) {
            length = page * chunk;
        } else {
            length = object.size();
        }

        CellStyle numberStyle = new CellStyle(CellStyle.HorizontalAlign.center);
        Table t = new Table(5, BorderStyle.UNICODE_DOUBLE_BOX,
                ShownBorders.ALL);

        t.setColumnWidth(0, 8, 16);
        t.setColumnWidth(1, 16, 32);
        t.setColumnWidth(2, 16, 32);
        t.setColumnWidth(3, 16, 32);
        t.setColumnWidth(4, 16, 32);

        t.addCell("ID", numberStyle);
        t.addCell("Name", numberStyle);
        t.addCell("Unit Price", numberStyle);
        t.addCell("Qty", numberStyle);
        t.addCell("Imported Date", numberStyle);
        for (int row = tmpRow; row < length; row++) {

            t.addCell("" + object.get(row).getId(), numberStyle);
            t.addCell("" + object.get(row).getName(), numberStyle);
            t.addCell("" + object.get(row).getUnitprice(), numberStyle);
            t.addCell("" + object.get(row).getQty(), numberStyle);
            t.addCell("" + object.get(row).getMyObj(), numberStyle);

        }
        Table t1 = new Table(2, BorderStyle.CLASSIC_LIGHT, ShownBorders.SURROUND);
        CellStyle right = new CellStyle(CellStyle.HorizontalAlign.right);
        CellStyle left = new CellStyle(CellStyle.HorizontalAlign.left);
        t1.setColumnWidth(0, 60, 100);
        t1.addCell(" Page: " + page + "/" + totalPage,left);
        t1.addCell("Total records: " + object.size()+" ",right);
        System.out.println(t.render());
        System.out.println(t1.render());
    }
    //       goto page
    public void Goto(int page) {

        Statement statement = null;
        try {
            getConnection().setAutoCommit(false);
            statement = getConnection().createStatement();

            //2- Select all data from tbl_setrow to display current row
            ResultSet resultSet = statement.executeQuery( "SELECT * FROM tbl_setrow ORDER BY id DESC \n" +
                    "LIMIT 1" );
            while ( resultSet.next() ) {
                int chunk = resultSet.getInt("row");

                int totalPage;
                if (object.size() % chunk != 0) {
                    totalPage = (object.size() / chunk) + 1;
                } else {
                    totalPage = (object.size() / chunk);
                }
                int length;

                int tmpRow = (page * chunk) - chunk;
                if (page != totalPage) {
                    length = page * chunk;
                } else {
                    length = object.size();
                }

                CellStyle numberStyle = new CellStyle(CellStyle.HorizontalAlign.center);
                Table t = new Table(5, BorderStyle.UNICODE_DOUBLE_BOX,
                        ShownBorders.ALL);

                t.setColumnWidth(0, 8, 16);
                t.setColumnWidth(1, 16, 32);
                t.setColumnWidth(2, 16, 32);
                t.setColumnWidth(3, 16, 32);
                t.setColumnWidth(4, 16, 32);

                t.addCell("ID", numberStyle);
                t.addCell("Name", numberStyle);
                t.addCell("Unit Price", numberStyle);
                t.addCell("Qty", numberStyle);
                t.addCell("Imported Date", numberStyle);
                for (int row = tmpRow; row < length; row++) {

                    t.addCell("" + object.get(row).getId(), numberStyle);
                    t.addCell("" + object.get(row).getName(), numberStyle);
                    t.addCell("" + object.get(row).getUnitprice(), numberStyle);
                    t.addCell("" + object.get(row).getQty(), numberStyle);
                    t.addCell("" + object.get(row).getMyObj(), numberStyle);

                }
                Table t1 = new Table(2, BorderStyle.CLASSIC_LIGHT, ShownBorders.SURROUND);
                CellStyle right = new CellStyle(CellStyle.HorizontalAlign.right);
                CellStyle left = new CellStyle(CellStyle.HorizontalAlign.left);
                t1.setColumnWidth(0, 60, 100);
                t1.addCell(" Page: " + page + "/" + totalPage,left);
                t1.addCell("Total records: " + object.size()+" ",right);
                System.out.println(t.render());
                System.out.println(t1.render());
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

    }
    //      Set row of display
    public void setRow(Connection connection){
        Statement statement = null;

        Scanner sc = new Scanner(System.in);

        int count=0,chunk=0;
        do {
            try {
                System.out.print("Please enter row for display: ");
                chunk = sc.nextInt();
                count = 0;
                if (chunk < 0)
                    throw new Exception();
            } catch (Exception e) {
                System.out.println("-> INPUT IS INVALID ! (Please Input Integer type)....");
                count++;
            }
            sc.nextLine();
        } while (count != 0);

        try {
            //1-Load Driver
//            Class.forName("org.postgresql.Driver");
//            connection = DriverManager
//                    .getConnection("jdbc:postgresql://localhost:5432/postgres",
//                            "postgres", "123");
            connection.setAutoCommit(false);
            statement = connection.createStatement();
            //3. Insert
            String sql = "INSERT INTO tbl_setrow (row) "
                    + "VALUES ('"+chunk+"');";
            statement.executeUpdate(sql);
            connection.commit();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        int totalPage;
        if (object.size() % chunk != 0) {
            totalPage = (object.size() / chunk) + 1;
        } else {
            totalPage = (object.size() / chunk);
        }

        int page = 1;
        int length;

        int tmpRow = 0;
        if (page != totalPage) {
            length = page * chunk;
        } else {
            length = object.size();
        }

        CellStyle numberStyle = new CellStyle(CellStyle.HorizontalAlign.center);
        Table t = new Table(5, BorderStyle.UNICODE_DOUBLE_BOX,
                ShownBorders.ALL);

        t.setColumnWidth(0, 8, 16);
        t.setColumnWidth(1, 16, 32);
        t.setColumnWidth(2, 16, 32);
        t.setColumnWidth(3, 16, 32);
        t.setColumnWidth(4, 16, 32);

        t.addCell("ID", numberStyle);
        t.addCell("Name", numberStyle);
        t.addCell("Unit Price", numberStyle);
        t.addCell("Qty", numberStyle);
        t.addCell("Imported Date", numberStyle);
        for (int row = tmpRow; row < length; row++) {

            t.addCell("" + object.get(row).getId(), numberStyle);
            t.addCell("" + object.get(row).getName(), numberStyle);
            t.addCell("" + object.get(row).getUnitprice(), numberStyle);
            t.addCell("" + object.get(row).getQty(), numberStyle);
            t.addCell("" + object.get(row).getMyObj(), numberStyle);

        }
        Table t1 = new Table(2, BorderStyle.CLASSIC_LIGHT, ShownBorders.SURROUND);
        CellStyle right = new CellStyle(CellStyle.HorizontalAlign.right);
        CellStyle left = new CellStyle(CellStyle.HorizontalAlign.left);
        t1.setColumnWidth(0, 60, 100);
        t1.addCell(" Page: " + page + "/" + totalPage,left);
        t1.addCell("Total records: " + object.size()+" ",right);
        System.out.println(t.render());
        System.out.println(t1.render());

    }
    //      Help guide
    public void help () {
        Table t = new Table(1, BorderStyle.CLASSIC_COMPATIBLE_WIDE, ShownBorders.SURROUND);
        t.setColumnWidth(0, 60, 150);
        t.addCell("1.    press      *: Display all record of products");
        t.addCell("2.    press      W: Add new product");
        t.addCell("3.    press      #w-{Name}-{Price}-{Qty} : Shorthand for Add new product");
        t.addCell("4.    press      R: Read Content any content");
        t.addCell("5.    press      $r-{ID}:  Shorthand for ReadByid");
        t.addCell("6.    press      U: Update data");
        t.addCell("7.    press      D: Delete data");
        t.addCell("8.    press      @d-{ID}: shorthand for Delete data");
        t.addCell("9.    press      F: Display First Page");
        t.addCell("10.    press      P: Display Previous Page");
        t.addCell("11.    press      N: Display Next Page");
        t.addCell("12.    press      L: Display Last Page");
        t.addCell("13.   press      S: Search product by name");
        t.addCell("14.   press      1k: for add 1k data");
        t.addCell("15.   press      1M: for add 1M data");
        t.addCell("16.   press      10M: for add 10M data");
        t.addCell("17.   press     Sa: Save record to file");
        t.addCell("18.   press     Ba: Backup data");
        t.addCell("19.   press     Re: Restore data");
        t.addCell("20.   press      H: Help");
        System.out.println(t.render());
    }
    //      insert 1k record
    public void Insert1kRecords(int data){
        long totalWriteTime = System.currentTimeMillis();
        long totalReadTime = System.currentTimeMillis();
        Iterator<StockDTO> it = object.iterator();
        while (it.hasNext()) {
            StockDTO tmp = it.next();
            it.remove();
        }
        for (int i = 1; i <= data; i++) {
            StockDTO a = new StockDTO(i, "Virus"+i, 2.3+i, 3+i);
            object.add(a);
        }
        int chunk = 3;
        int totalPage;
        if (object.size() % chunk != 0) {
            totalPage = (object.size() / chunk) + 1;
        } else {
            totalPage = (object.size() / chunk);
        }

        int page = 1;
        int length;

        int tmpRow = (page * chunk) - chunk;
        if (page != totalPage) {
            length = page * chunk;
        } else {
            length = object.size();
        }
        System.out.println("Total Write Time: "+(System.currentTimeMillis()-totalWriteTime));
        System.out.println("Total Read Time: "+(System.currentTimeMillis()-totalReadTime));
        CellStyle numberStyle = new CellStyle(CellStyle.HorizontalAlign.center);
        Table t = new Table(5, BorderStyle.UNICODE_DOUBLE_BOX,
                ShownBorders.ALL);

        t.setColumnWidth(0, 8, 16);
        t.setColumnWidth(1, 16, 32);
        t.setColumnWidth(2, 16, 32);
        t.setColumnWidth(3, 16, 32);
        t.setColumnWidth(4, 16, 32);

        t.addCell("ID",numberStyle);
        t.addCell("Name",numberStyle);
        t.addCell("Unit Price",numberStyle);
        t.addCell("Qty",numberStyle);
        t.addCell("Imported Date",numberStyle);
        for (int row = tmpRow; row < length; row++) {

            t.addCell("" + object.get(row).getId(), numberStyle);
            t.addCell("" + object.get(row).getName(), numberStyle);
            t.addCell("" + object.get(row).getUnitprice(), numberStyle);
            t.addCell("" + object.get(row).getQty(), numberStyle);
            t.addCell("" + object.get(row).getMyObj(), numberStyle);

        }
        Table t1 = new Table(2, BorderStyle.CLASSIC_LIGHT, ShownBorders.SURROUND);
        CellStyle right = new CellStyle(CellStyle.HorizontalAlign.right);
        CellStyle left = new CellStyle(CellStyle.HorizontalAlign.left);
        t1.setColumnWidth(0, 60, 100);
        t1.addCell(" Page: " + page + "/" + totalPage,left);
        t1.addCell("Total records: " + object.size()+" ",right);
        System.out.println(t.render());
        System.out.println(t1.render());
    }
    //      insert 1M
    public void Insert1MRecords(int data){
        long totalWriteTime = System.currentTimeMillis();
        long totalReadTime = System.currentTimeMillis();
        Iterator<StockDTO> it = object.iterator();
        while (it.hasNext()) {
            StockDTO tmp = it.next();
            it.remove();
        }
        for (int i = 1; i <= data; i++) {
            StockDTO a = new StockDTO(i, "Virus", 2.3, 3);
            object.add(a);
        }
        int chunk = 3;
        int totalPage;
        if (object.size() % chunk != 0) {
            totalPage = (object.size() / chunk) + 1;
        } else {
            totalPage = (object.size() / chunk);
        }

        int page = 1;
        int length;

        int tmpRow = (page * chunk) - chunk;
        if (page != totalPage) {
            length = page * chunk;
        } else {
            length = object.size();
        }
        System.out.println("Total Write Time: "+(System.currentTimeMillis()-totalWriteTime));
        System.out.println("Total Read Time: "+(System.currentTimeMillis()-totalReadTime));
        CellStyle numberStyle = new CellStyle(CellStyle.HorizontalAlign.center);
        Table t = new Table(5, BorderStyle.UNICODE_DOUBLE_BOX,
                ShownBorders.ALL);

        t.setColumnWidth(0, 8, 16);
        t.setColumnWidth(1, 16, 32);
        t.setColumnWidth(2, 16, 32);
        t.setColumnWidth(3, 16, 32);
        t.setColumnWidth(4, 16, 32);

        t.addCell("ID",numberStyle);
        t.addCell("Name",numberStyle);
        t.addCell("Unit Price",numberStyle);
        t.addCell("Qty",numberStyle);
        t.addCell("Imported Date",numberStyle);
        for (int row = tmpRow; row < length; row++) {

            t.addCell("" + object.get(row).getId(), numberStyle);
            t.addCell("" + object.get(row).getName(), numberStyle);
            t.addCell("" + object.get(row).getUnitprice(), numberStyle);
            t.addCell("" + object.get(row).getQty(), numberStyle);
            t.addCell("" + object.get(row).getMyObj(), numberStyle);

        }
        Table t1 = new Table(2, BorderStyle.CLASSIC_LIGHT, ShownBorders.SURROUND);
        CellStyle right = new CellStyle(CellStyle.HorizontalAlign.right);
        CellStyle left = new CellStyle(CellStyle.HorizontalAlign.left);
        t1.setColumnWidth(0, 60, 100);
        t1.addCell(" Page: " + page + "/" + totalPage,left);
        t1.addCell("Total records: " + object.size()+" ",right);
        System.out.println(t.render());
        System.out.println(t1.render());
    }
    //      insert 10M
    public void Insert10MRecords(int data){
        long totalWriteTime = System.currentTimeMillis();
        long totalReadTime = System.currentTimeMillis();
        Iterator<StockDTO> it = object.iterator();
        while (it.hasNext()) {
            StockDTO tmp = it.next();
            it.remove();
        }
        for (int i = 1; i <= data; i++) {
            StockDTO a = new StockDTO(i, "Virus", 2.3, 3);
            object.add(a);
        }
        int chunk = 3;
        int totalPage;
        if (object.size() % chunk != 0) {
            totalPage = (object.size() / chunk) + 1;
        } else {
            totalPage = (object.size() / chunk);
        }

        int page = 1;
        int length;

        int tmpRow = (page * chunk) - chunk;
        if (page != totalPage) {
            length = page * chunk;
        } else {
            length = object.size();
        }
        System.out.println("Total Write Time: "+(System.currentTimeMillis()-totalWriteTime));
        System.out.println("Total Read Time: "+(System.currentTimeMillis()-totalReadTime));
        CellStyle numberStyle = new CellStyle(CellStyle.HorizontalAlign.center);
        Table t = new Table(5, BorderStyle.UNICODE_DOUBLE_BOX,
                ShownBorders.ALL);

        t.setColumnWidth(0, 8, 16);
        t.setColumnWidth(1, 16, 32);
        t.setColumnWidth(2, 16, 32);
        t.setColumnWidth(3, 16, 32);
        t.setColumnWidth(4, 16, 32);

        t.addCell("ID",numberStyle);
        t.addCell("Name",numberStyle);
        t.addCell("Unit Price",numberStyle);
        t.addCell("Qty",numberStyle);
        t.addCell("Imported Date",numberStyle);
        for (int row = tmpRow; row < length; row++) {

            t.addCell("" + object.get(row).getId(), numberStyle);
            t.addCell("" + object.get(row).getName(), numberStyle);
            t.addCell("" + object.get(row).getUnitprice(), numberStyle);
            t.addCell("" + object.get(row).getQty(), numberStyle);
            t.addCell("" + object.get(row).getMyObj(), numberStyle);

        }
        Table t1 = new Table(2, BorderStyle.CLASSIC_LIGHT, ShownBorders.SURROUND);
        CellStyle right = new CellStyle(CellStyle.HorizontalAlign.right);
        CellStyle left = new CellStyle(CellStyle.HorizontalAlign.left);
        t1.setColumnWidth(0, 60, 100);
        t1.addCell(" Page: " + page + "/" + totalPage,left);
        t1.addCell("Total records: " + object.size()+" ",right);
        System.out.println(t.render());
        System.out.println(t1.render());
    }
}