package SRG3.Controller;

import SRG3.Model.DAO.StockDAO;
import SRG3.Model.DTO.StockDTO;
import SRG3.View.StockView;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Iterator;

public class StockController {
    private StockDAO stockDAO;
    private StockView stockView;
    public StockController(StockDAO stockDAO,StockView stockView){
        this.stockDAO=stockDAO;
        this.stockView=stockView;
    }
    //  Print Arrayslist of stock
    public void PrintallStock(){
        Connection connection= stockDAO.getConnection();
        ArrayList object= (ArrayList) stockDAO.Display();
        stockView.getallStock(object,connection);
    }
    //    Add list data to database
    public void SaveinDB(){
        Connection connection= stockDAO.getConnection();
        stockDAO.Savedata(connection);
    }
    //    Backup table
    public void Backuptable(){
        Connection connection= stockDAO.getConnection();
        stockDAO.Backup(connection);
    }
    //    Restore Table
    public void Restoretable(){
        ArrayList tbn= (ArrayList) stockDAO.Displaytbn();
        Connection connection= stockDAO.getConnection();
        stockDAO.Restore(connection);
        stockView.pritntablename(tbn);
    }
    //    Insert new stock in list

    //    Add list data to database
    public void InputStock(){
        ArrayList object= (ArrayList) stockDAO.Display();
        StockDTO stockDTO=stockView.getUserinput(object);
        stockDAO.Writedata(stockDTO);
    }
    //    Insert shorthand
    public void inputshorthand(String name,String[] arrOfStr){
        ArrayList object= (ArrayList) stockDAO.Display();
        StockDTO stockDTO=stockView.getdatashorthand(name,arrOfStr,object);
        stockDAO.Writeshorthand(stockDTO);
    }
    //    ReadbyID
    public void ReadbyID(){
        int id=stockView.getIDtosearch();
        stockDAO.SearchbyID(id);
    }
    //    ReadbyID Shorthand
    public void ReadbyIDshorthand(String[] arrOfStr){
        int id=stockView.getIDsearchshorthand(arrOfStr);
        stockDAO.SearchbyID(id);
    }
    //    Delete by id
    public void Delete(){
        ArrayList object= (ArrayList) stockDAO.Display();
        int id=stockView.getIDtoDelete();
        Iterator<StockDTO> it = stockDAO.DeletebyID(id);
    }
    //    Delete byid Shorthand
    public void Deleteshorthand(String[] arrOfStr){
        ArrayList object= (ArrayList) stockDAO.Display();
        int id=stockView.getIDdeleteshorthand(arrOfStr);
        Iterator<StockDTO> it= stockDAO.DeletebyID(id);
    }
    //      Search By name
    public void SearchbyName(){
        String name=stockView.getNametosearch();
        stockDAO.SearchbyName(name);
    }
    //    Update
    public void Update(){
        ArrayList object= (ArrayList) stockDAO.Display();
        int id=stockView.getIDtoupdate();
        stockDAO.Searchidupdate(id);
    }

    //    firstpage
    public void Firstpage(){
        stockDAO.FirstPage();
    }
    public void PreviousPage(int p){
        stockDAO.previousPage(p);
    }
    public void Nextpage(int p){
        stockDAO.nextPage(p);
    }
    public void Lastpage(){
        stockDAO.lastPage();
    }
    public void Gotopage(int p){
        stockDAO.Goto(p);
    }
    public void setrow(){
        Connection connection= stockDAO.getConnection();
        stockDAO.setRow(connection);
    }
    public void Helpguide(){
        stockDAO.help();
    }
    public void Insert1k(int data){
        stockDAO.Insert1kRecords(data);
    }
    public void Insert1m(int data){
        stockDAO.Insert1MRecords(data);
    }
    public void Insert10m(int data){
        stockDAO.Insert10MRecords(data);
    }

}
