package SRG3.View;

import SRG3.Controller.StockController;
import SRG3.Model.DAO.StockDAO;
import SRG3.showHeader;
import org.nocrala.tools.texttablefmt.BorderStyle;
import org.nocrala.tools.texttablefmt.CellStyle;
import org.nocrala.tools.texttablefmt.ShownBorders;
import org.nocrala.tools.texttablefmt.Table;

import java.io.IOException;
import java.util.Iterator;
import java.util.Scanner;

public class Displaymenu {
    StockDAO stockDAO=new StockDAO();
    StockView stockView=new StockView();

    StockController stockController=new StockController(stockDAO,stockView);
    public void menu() throws IOException {
        int f=1,pre=0,p=1,recover=0;
        Scanner cin=new Scanner(System.in);
        showHeader.showHeader();
        do {
            System.out.println("╔══════════════════════════════════════════════════════════════════════════════════════════╗");
            System.out.println("║ *)Display | W)rite | R)ead    | U)pdate | D)elete | F)irst   | P)revious | N)ext | L)ast ║");
            System.out.println("║ S)each    | G)oto  | Se)t row | Sa)ve   | Ba)ckup | Re)store | He)lp     | E)xit         ║");
            System.out.println("╚══════════════════════════════════════════════════════════════════════════════════════════╝");
            String ch;
            System.out.print("\n"+"Commend-> : ");
            ch = cin.nextLine();
            String[] arrOfStr = ch.split("-");
            char c=ch.charAt(0);
            if(c=='#')
                ch="ws";
            else if(c=='$')
                ch="rs";
            else if(c=='@')
                ch="ds";
            switch (ch) {
                case "*":
                    try{
                        stockController.PrintallStock();
                    }catch (Exception e){
                        System.out.println("=> Please Connect Your local Database and Initialize some Data in 'StockDAO'....");
                        System.exit(0);
                    }
                    break;
                case "w":
                case "W":
                    recover++;
                    stockController.InputStock();
                    break;
                case "ws":
                    recover++;
                    String name=null;
                    stockController.inputshorthand(name,arrOfStr);
                    break;
                case "r":
                case "R":
                    stockController.ReadbyID();
                    break;
                case "rs":
//                    int id=0,count=0;
//                    try{
//                        id=Integer.parseInt(arrOfStr[1]);
//                    }catch (Exception e){
//                        System.out.println("-> YOUR DATA IS INVALID !");
//                    }
//                    for(Stock sk:object){
//                        if(id==sk.getId()){
//                            CellStyle numberStyle20 = new CellStyle(CellStyle.HorizontalAlign.left);
//                            Table t20 = new Table(1, BorderStyle.UNICODE_DOUBLE_BOX, ShownBorders.SURROUND);
//                            t20.setColumnWidth( 0,32,32);
//
//                            t20.addCell(" ID :"+sk.getId(),numberStyle20);
//                            t20.addCell( " Name :"+sk.getName(), numberStyle20);
//                            t20.addCell(" UnitPrice :"+sk.getUnitprice(),numberStyle20);
//                            t20.addCell(" Qty :"+sk.getQty(),numberStyle20);
//                            t20.addCell(" Date :"+sk.getMyObj(),numberStyle20);
//
//                            System.out.println(t20.render());
//                            count++;
//                        }
//                    }
//                    if(count==0){
//                        System.out.println("-> Your ID id EXIST....");
//                    }
                    stockController.ReadbyIDshorthand(arrOfStr);
                    break;
                case "u":
                case "U":
                    recover++;
                    stockController.Update();
                    break;
                case "d":
                case "D":
                    recover++;
                    stockController.Delete();
                    break;
                case "ds":
                    recover++;
                    stockController.Deleteshorthand(arrOfStr);
                    break;
                case "f":
                case "F":
                    stockController.Firstpage();
                    f = 1;
                    break;
                case "p":
                case "P":
                    if(p==1){
                        stockController.PreviousPage(p);
                    }
                    else if(p >= 1) {
                        p--;
                        stockController.PreviousPage(p);
                    }
                    break;
                case "n":
                case "N":
                    if(f==1) {
                        f++;
                        stockController.Nextpage(f);
                    }else if(p>=1){
                        p++;
                        stockController.Nextpage(p);
                    }
                    break;
                case "l":
                case "L":
                    stockController.Lastpage();
                    break;
                case "s":
                case "S":
                    stockController.SearchbyName();
                    break;
                case "g":
                case "G":
                    int pa=0,ct=0;
                    do {
                        try {
                            System.out.print("-> Go to page: ");
                            pa = cin.nextInt();
                            ct = 0;
                            if (pa < 0)
                                throw new Exception();
                        } catch (Exception e) {
                            System.out.println("-> INPUT IS INVALID ! (Please Input Integer type)....");
                            ct++;
                        }
                        cin.nextLine();
                    } while (ct != 0);
                    p=pa;
                    stockController.Gotopage(pa);
                    break;
                case "se":
                case "Se":
                    stockController.setrow();
                    break;
                case "sa":
                case "Sa":
                    recover=0;
                    stockController.SaveinDB();
                    break;
                case "ba":
                case "Ba":
                    stockController.Backuptable();
                    break;
                case "re":
                case "Re":
                    stockController.Restoretable();
                    break;
                case "he":
                case "He":
                    stockController.Helpguide();
                    break;
                case "1k":
                case "1K":
                    stockController.Insert1k(1000);
                    break;
                case "1m":
                case "1M":
                    stockController.Insert1m(1000000);
                    break;
                case "10m":
                case "10M":
                    stockController.Insert10m(10000000);
                    break;
                case "ex":
                case "Ex":
                    char veri;
                    if( recover!=0){
                        System.out.print("Do you want to save your data ? [Y/y] or [N/n] : ");
                        veri=cin.next().charAt(0);
                        while (!(veri=='y'||veri=='Y'||veri=='n'||veri=='N')) {
                            System.out.print("Are you sure want to add this record ? [Y/y] or [N/n] : ");
                            veri=cin.next().charAt(0);
                        }
                        if (veri == 'y' || veri == 'Y') {
                            stockController.SaveinDB();
                        } else {
                            System.out.println("Your chose is cancel");
                        }
                    }
                    System.out.println("--good bye-- ):");
                    System.exit(0);
                    break;
                default:
                    System.out.println("-> INPUT IS INVALID ! (Please Input Follow Menu)....");
            }
        }while(true);

    }

}
