package SRG3.View;

import SRG3.Model.DTO.StockDTO;

import java.sql.*;
import java.util.Iterator;
import java.util.List;

import SRG3.Regex;
import org.nocrala.tools.texttablefmt.BorderStyle;
import org.nocrala.tools.texttablefmt.CellStyle;
import org.nocrala.tools.texttablefmt.ShownBorders;
import org.nocrala.tools.texttablefmt.Table;
import java.util.Scanner;
public class StockView {
    Scanner cin = new Scanner(System.in);

    //    display Table Data with Table
    public void getallStock(List<StockDTO> object,Connection connection) {
//        Connection connection = null;
        Statement statement = null;
        int a = 0;
        //1-Load Driver
        try {
//            Class.forName("org.postgresql.Driver");
//            connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/DB2", "postgres", "Rathkhmer2021");
            connection.setAutoCommit(false);
            statement = connection.createStatement();

            //2- Select all data from tbl_setrow to display current row
            ResultSet resultSet = statement.executeQuery( "SELECT * FROM tbl_setrow ORDER BY id DESC LIMIT 1" );

            while ( resultSet.next() ) {

                    int chunk = resultSet.getInt("row");//chunk=setrow

                    int totalPage;
                    if (object.size() % chunk != 0) {
                        totalPage = (object.size() / chunk) + 1;
                    } else {
                        totalPage = (object.size() / chunk);
                    }
                    int page = 1;
                    int length;

                    int tmpRow = (page * chunk) - chunk;
                    if (page != totalPage) {
                        length = page * chunk;
                    } else {
                        length = object.size();
                    }


                    CellStyle numberStyle = new CellStyle(CellStyle.HorizontalAlign.center);
                    Table t = new Table(5, BorderStyle.UNICODE_DOUBLE_BOX,
                            ShownBorders.ALL);

                    t.setColumnWidth(0, 8, 16);
                    t.setColumnWidth(1, 16, 32);
                    t.setColumnWidth(2, 16, 32);
                    t.setColumnWidth(3, 16, 32);
                    t.setColumnWidth(4, 16, 32);

                    t.addCell("ID", numberStyle);
                    t.addCell("Name", numberStyle);
                    t.addCell("Unit Price", numberStyle);
                    t.addCell("Qty", numberStyle);
                    t.addCell("Imported Date", numberStyle);
                    for (int row = tmpRow; row < length; row++) {

                        t.addCell("" + object.get(row).getId(), numberStyle);
                        t.addCell("" + object.get(row).getName(), numberStyle);
                        t.addCell("" + object.get(row).getUnitprice(), numberStyle);
                        t.addCell("" + object.get(row).getQty(), numberStyle);
                        t.addCell("" + object.get(row).getMyObj(), numberStyle);

                    }
                    Table t1 = new Table(2, BorderStyle.CLASSIC_LIGHT, ShownBorders.SURROUND);
                    CellStyle right = new CellStyle(CellStyle.HorizontalAlign.right);
                    CellStyle left = new CellStyle(CellStyle.HorizontalAlign.left);
                    t1.setColumnWidth(0, 60, 100);
                    t1.addCell(" Page: " + page + "/" + totalPage, left);
                    t1.addCell("Total records: " + object.size() + " ", right);
                    System.out.println(t.render());
                    System.out.println(t1.render());
            }

            //after setrow, automatic reset table from db
            statement = connection.createStatement();
//            String sqlDel = "DELETE FROM tbl_setrow\n" +
//                    "WHERE id IN (SELECT id\n" +
//                    "                         FROM tbl_setrow\n" +
//                    "                         ORDER BY id asc\n" +
//                    "                         LIMIT 1)";
//            statement.executeUpdate(sqlDel);
            connection.commit();

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    //    get all table name
    public String pritntablename(List<String> tablename) {
        int ch;
        String tbname=null;
        String name[]=new String[1000];
        System.out.println("-------Restore your table-------");
        for (int i = 0; i < tablename.size(); i++) {
            System.out.println(i + "." + tablename.get(i));
            name[i]=tablename.get(i);
        }
        System.out.print("-> Choose your Table to restore : ");
        ch = cin.nextInt();
        switch (ch) {
            case 0:
                tbname=name[0];
                break;
            case 1:
                break;
        }
        return tbname;
    }

    //    Read inputing for keybord
    public StockDTO getUserinput(List<StockDTO> object) {
        int id, qty = 0, autoid = 0;
        String name;
        Double unitprice = 0.0;
        int count = 0;
        autoid = object.size() + 1;
        id = autoid;
        System.out.println("=> Product ID      :  " + id);
        do {
            System.out.print("=> Product's Name    :  ");
            name = cin.nextLine();
            if (Regex.onlystring(name))
                System.out.println("-> INPUT IS INVALID ! (Please Input String type)....");
        } while (Regex.onlystring(name));
        do {
            try {
                System.out.print("=> Product's Price  :  ");
                unitprice = cin.nextDouble();
                count = 0;
                if (unitprice < 0) {
                    throw new Exception();
                }
            } catch (Exception e) {
                System.out.println("-> INPUT IS INVALID ! (Please Input Double type)....");
                count++;
            }
            cin.nextLine();
        } while (count != 0);
        do {
            try {
                System.out.print("=> Product's Qty      :  ");
                qty = cin.nextInt();
                count = 0;
                if (qty < 0)
                    throw new Exception();
            } catch (Exception e) {
                System.out.println("-> INPUT IS INVALID ! (Please Input Integer type)....");
                count++;
            }
            cin.nextLine();
        } while (count != 0);
        return new StockDTO(id, name, unitprice, qty);
    }
    public StockDTO getdatashorthand(String name,String[] arrOfStr,List<StockDTO> object){
        int autoid=0;
        int qty=0;
        double pric=0;
        autoid = object.size() + 1;
        try{
            name=arrOfStr[1];
        }catch (Exception e){
            System.out.println("-> YOUR DATA IS INVALID !");
        }
        try {
            pric=Double.parseDouble(arrOfStr[2]);
        }catch (Exception e){
            System.out.println("-> YOUR DATA IS INVALID !");
        }
        try {
            qty= Integer.parseInt(arrOfStr[3]);
        }catch (Exception e){
            System.out.println("-> YOUR DATA IS INVALID !");
        }
        return new StockDTO(autoid,name,pric,qty);
    }

    public void verifyInsertshort(StockDTO in) {
        CellStyle numberStyle10 = new CellStyle(CellStyle.HorizontalAlign.left);
        Table t10 = new Table(1, BorderStyle.UNICODE_DOUBLE_BOX, ShownBorders.SURROUND);
        t10.setColumnWidth( 0,32,32);
        t10.addCell(" ID :"+in.getId(),numberStyle10);
        t10.addCell( " Name :"+in.getName(), numberStyle10);
        t10.addCell(" UnitPrice :"+in.getUnitprice(),numberStyle10);
        t10.addCell(" Qty :"+in.getQty(),numberStyle10);
        t10.addCell(" Date :"+in.getMyObj(),numberStyle10);

        System.out.println(t10.render());
    }
    //    verify insert
    public void verifyInsert(StockDTO st,List<StockDTO> object){
        char veri;
        System.out.print("Are you sure want to add this record ? [Y/y] or [N/n] : ");
        veri=cin.next().charAt(0);
        while (!(veri=='y'||veri=='Y'||veri=='n'||veri=='N')) {
            System.out.print("Are you sure want to add this record ? [Y/y] or [N/n] : ");
            veri=cin.next().charAt(0);
        }
        if (veri == 'y' || veri == 'Y') {
            object.add(st);
            System.out.println("=> Add To List Successfully !");
        } else {
            System.out.println("=> Your choice is Cancel !");
        }
        cin.nextLine();
    }

    //    Read id to search
    public int getIDtosearch() {
        int id = 0, count = 0;
        do {
            try {
                System.out.print("=> Enter ID Of Product      :  ");
                id = cin.nextInt();
                count = 0;
                if (id < 0)
                    throw new Exception();
            } catch (Exception e) {
                System.out.println("-> INPUT IS INVALID ! (Please Input Integer type)....");
                count++;
            }
            cin.nextLine();
        } while (count != 0);
        return id;
    }
    //    get id to search shorthand
    public int getIDsearchshorthand(String[] arrOfStr){
        int id=0,count=0;
        try{
            id=Integer.parseInt(arrOfStr[1]);
        }catch (Exception e){
            System.out.println("-> YOUR DATA IS INVALID !");
        }
        return id;
    }


    //   Read id to delete
    public int getIDtoDelete() {
        int id = 0, count = 0;
        do {
            try {
                System.out.print("=> Enter ID Of Product       :  ");
                id = cin.nextInt();
                count = 0;
                if (id < 0)
                    throw new Exception();
            } catch (Exception e) {
                System.out.println("-> INPUT IS INVALID ! (Please Input Integer type)....");
                count++;
            }
            cin.nextLine();
        } while (count != 0);
        return id;
    }
    //    Read id to delete shorthand
    public int getIDdeleteshorthand(String[] arrOfStr){
        int id=0,cu=0;
        try{
            id=Integer.parseInt(arrOfStr[1]);
        }catch (Exception e){
            System.out.println("-> YOUR DATA IS INVALID !");
        }
        return id;
    }

    public void verifyDelete(Iterator it) {
        char veri;
        System.out.print("Are you sure want to add this record ? [Y/y] or [N/n] : ");
        veri = cin.next().charAt(0);
        while (!(veri == 'y' || veri == 'Y' || veri == 'n' || veri == 'N')) {
            System.out.print("Are you sure want to add this record ? [Y/y] or [N/n] : ");
            veri = cin.next().charAt(0);
        }
        if (veri == 'y' || veri == 'Y') {
            it.remove();
            System.out.println("=> Remove Successfully !");
        } else {
            System.out.println("Your choice is Cancel !");
        }
    }

    // Read Name user input
    public String getNametosearch() {
        String search;
        int count = 0;
        do {
            System.out.print("=> Enter Name to Search    :  ");
            search = cin.nextLine();
            if (Regex.onlystring(search))
                System.out.println("-> INPUT IS INVALID ! (Please Input String type)....");
        } while (Regex.onlystring(search));
        return search;
    }

    //   Read id to update record
    public int getIDtoupdate(){
        int id=0,count=0;
        do {
            try {
                System.out.print("=> Enter ID Of Product       :  ");
                id = cin.nextInt();
                count = 0;
                if (id < 0)
                    throw new Exception();
            } catch (Exception e) {
                System.out.println("-> INPUT IS INVALID ! (Please Input Integer type)....");
                count++;
            }
            cin.nextLine();
        } while (count != 0);
        return id;
    }
    //    Menu for choise of Update
    public void menuUpdate(int id,List<StockDTO> students) {
        int ch = 0, count = 0;
        System.out.println("What do you want to update ?");
        System.out.println("╔════════════════════════════════════════════════════════════════════════════╗");
        System.out.println("║ 1. All     2. Name     3.Quantity      4.Unit Price        5.Back to Menu  ║");
        System.out.println("╚════════════════════════════════════════════════════════════════════════════╝");
        do {
            try {
                System.out.print("=> Choose Option : ");
                ch = cin.nextInt();
                count = 0;
                if (ch < 0)
                    throw new Exception();
            } catch (Exception e) {
                System.out.println("-> INPUT IS INVALID ! (Please Input Follow Menu)....");
                count++;
            }
            cin.nextLine();
        } while (count != 0);
        switch (ch) {
            case 1:
                updateAll(id,students);
                char veri;
                for (StockDTO stockDTO : students) {
                    if (id == stockDTO.getId()) {
                        CellStyle numberStyle = new CellStyle(CellStyle.HorizontalAlign.left);
                        Table t = new Table(1, BorderStyle.UNICODE_DOUBLE_BOX, ShownBorders.SURROUND);
                        t.setColumnWidth(0, 32, 32);

                        t.addCell(" ID :" + stockDTO.getId(), numberStyle);
                        t.addCell(" Name :" + stockDTO.getName(), numberStyle);
                        t.addCell(" UnitPrice :" + stockDTO.getUnitprice(), numberStyle);
                        t.addCell(" Qty :" + stockDTO.getQty(), numberStyle);
                        t.addCell(" Date :" + stockDTO.getMyObj(), numberStyle);

                        System.out.println(t.render());
                        System.out.print("Are you sure want to add this record ? [Y/y] or [N/n] : ");
                        veri = cin.next().charAt(0);
                        while (!(veri == 'y' || veri == 'Y' || veri == 'n' || veri == 'N')) {
                            System.out.print("Are you sure want to add this record ? [Y/y] or [N/n] : ");
                            veri = cin.next().charAt(0);
                        }
                        if (veri == 'y' || veri == 'Y') {
                            System.out.println("=> Update Successfully !");
                        } else {
                            System.out.println("Your choice is Cancel !");
                        }
                        cin.nextLine();
                    }
                }
                break;
            case 2:
                updateName(id,students);
                for (StockDTO stockDTO : students) {
                    if (id == stockDTO.getId()) {
                        CellStyle numberStyle = new CellStyle(CellStyle.HorizontalAlign.left);
                        Table t = new Table(1, BorderStyle.UNICODE_DOUBLE_BOX, ShownBorders.SURROUND);
                        t.setColumnWidth(0, 32, 32);

                        t.addCell(" ID :" + stockDTO.getId(), numberStyle);
                        t.addCell(" Name :" + stockDTO.getName(), numberStyle);
                        t.addCell(" UnitPrice :" + stockDTO.getUnitprice(), numberStyle);
                        t.addCell(" Qty :" + stockDTO.getQty(), numberStyle);
                        t.addCell(" Date :" + stockDTO.getMyObj(), numberStyle);

                        System.out.println(t.render());
                        System.out.print("Are you sure want to add this record ? [Y/y] or [N/n] : ");
                        veri = cin.next().charAt(0);
                        while (!(veri == 'y' || veri == 'Y' || veri == 'n' || veri == 'N')) {
                            System.out.print("Are you sure want to add this record ? [Y/y] or [N/n] : ");
                            veri = cin.next().charAt(0);
                        }
                        if (veri == 'y' || veri == 'Y') {
                            System.out.println("=> Update Successfully !");
                        } else {
                            System.out.println("Your choice is Cancel !");
                        }
                        cin.nextLine();
                    }
                }
                break;
            case 3:
                updateUnitprice(id,students);
                for (StockDTO stockDTO : students) {
                    if (id == stockDTO.getId()) {
                        CellStyle numberStyle = new CellStyle(CellStyle.HorizontalAlign.left);
                        Table t = new Table(1, BorderStyle.UNICODE_DOUBLE_BOX, ShownBorders.SURROUND);
                        t.setColumnWidth(0, 32, 32);

                        t.addCell(" ID :" + stockDTO.getId(), numberStyle);
                        t.addCell(" Name :" + stockDTO.getName(), numberStyle);
                        t.addCell(" UnitPrice :" + stockDTO.getUnitprice(), numberStyle);
                        t.addCell(" Qty :" + stockDTO.getQty(), numberStyle);
                        t.addCell(" Date :" + stockDTO.getMyObj(), numberStyle);

                        System.out.println(t.render());
                        System.out.print("Are you sure want to add this record ? [Y/y] or [N/n] : ");
                        veri = cin.next().charAt(0);
                        while (!(veri == 'y' || veri == 'Y' || veri == 'n' || veri == 'N')) {
                            System.out.print("Are you sure want to add this record ? [Y/y] or [N/n] : ");
                            veri = cin.next().charAt(0);
                        }
                        if (veri == 'y' || veri == 'Y') {
                            System.out.println("=> Update Successfully !");
                        } else {
                            System.out.println("Your choice is Cancel !");
                        }
                        cin.nextLine();
                    }
                }
                break;
            case 4:
                updateQty(id,students);
                for (StockDTO stockDTO : students) {
                    if (id == stockDTO.getId()) {
                        CellStyle numberStyle = new CellStyle(CellStyle.HorizontalAlign.left);
                        Table t = new Table(1, BorderStyle.UNICODE_DOUBLE_BOX, ShownBorders.SURROUND);
                        t.setColumnWidth(0, 32, 32);

                        t.addCell(" ID :" + stockDTO.getId(), numberStyle);
                        t.addCell(" Name :" + stockDTO.getName(), numberStyle);
                        t.addCell(" UnitPrice :" + stockDTO.getUnitprice(), numberStyle);
                        t.addCell(" Qty :" + stockDTO.getQty(), numberStyle);
                        t.addCell(" Date :" + stockDTO.getMyObj(), numberStyle);

                        System.out.println(t.render());
                        System.out.print("Are you sure want to add this record ? [Y/y] or [N/n] : ");
                        veri = cin.next().charAt(0);
                        while (!(veri == 'y' || veri == 'Y' || veri == 'n' || veri == 'N')) {
                            System.out.print("Are you sure want to add this record ? [Y/y] or [N/n] : ");
                            veri = cin.next().charAt(0);
                        }
                        if (veri == 'y' || veri == 'Y') {
                            System.out.println("=> Update Successfully !");
                        } else {
                            System.out.println("Your choice is Cancel !");
                        }
                        cin.nextLine();
                    }
                }
                break;
            case 5:
                break;
            default:
                System.out.println("-> INPUT IS INVALID ! (Please Input Follow Menu)....");
        }

    }
    // update All field
    public void updateAll(int idd,List<StockDTO> students){
        int count = 0;
        int id, qty;
        String name;
        double unitprice;
        for(StockDTO st:students) {
            if (idd == st.getId()) {
                do {
                    System.out.print("=> Enter Product Name    :  ");
                    name = cin.nextLine();
                    st.setName(name);
                    if (Regex.onlystring(name))
                        System.out.println("-> INPUT IS INVALID ! (Please Input String type)....");
                } while (Regex.onlystring(name));
                do {
                    try {
                        System.out.print("=> Enter UnitPrice  :  ");
                        unitprice = cin.nextDouble();
                        st.setUnitprice(unitprice);
                        count = 0;
                        if (unitprice < 0) {
                            throw new Exception();
                        }
                    } catch (Exception e) {
                        System.out.println("-> INPUT IS INVALID ! (Please Input Double type)....");
                        count++;
                    }
                    cin.nextLine();
                } while (count != 0);
                do {
                    try {
                        System.out.print("=> Enter QTY      :  ");
                        qty = cin.nextInt();
                        st.setQty(qty);
                        count = 0;
                        if (qty < 0)
                            throw new Exception();
                    } catch (Exception e) {
                        System.out.println("-> INPUT IS INVALID ! (Please Input Integer type)....");
                        count++;
                    }
                    cin.nextLine();
                } while (count != 0);
            }
        }
    }
    // update Name
    public void updateName(int idd,List<StockDTO> students){
        int count = 0;
        int id, qty;
        String name;
        double unitprice;
        for(StockDTO st:students) {
            if (idd == st.getId()) {
                do {
                    System.out.print("=> Enter Product Name    :  ");
                    name = cin.nextLine();
                    st.setName(name);
                    if (Regex.onlystring(name))
                        System.out.println("-> INPUT IS INVALID ! (Please Input String type)....");
                } while (Regex.onlystring(name));
            }
        }
    }
    // update Unitprice
    public void updateUnitprice(int idd,List<StockDTO> students){
        int count = 0;
        int id, qty;
        String name;
        double unitprice;
        for(StockDTO st:students) {
            if (idd == st.getId()) {
                do {
                    try {
                        System.out.print("=> Enter UnitPrice  :  ");
                        unitprice = cin.nextDouble();
                        st.setUnitprice(unitprice);
                        count = 0;
                        if (unitprice < 0) {
                            throw new Exception();
                        }
                    } catch (Exception e) {
                        System.out.println("-> INPUT IS INVALID ! (Please Input Double type)....");
                        count++;
                    }
                    cin.nextLine();
                } while (count != 0);
            }
        }
    }
    // update Qty
    public void updateQty(int idd,List<StockDTO> students){
        int count = 0;
        int id, qty;
        String name;
        double unitprice;
        for(StockDTO st:students) {
            if (idd == st.getId()) {
                do {
                    try {
                        System.out.print("=> Enter QTY      :  ");
                        qty = cin.nextInt();
                        st.setQty(qty);
                        count = 0;
                        if (qty < 0)
                            throw new Exception();
                    } catch (Exception e) {
                        System.out.println("-> INPUT IS INVALID ! (Please Input Integer type)....");
                        count++;
                    }
                    cin.nextLine();
                } while (count != 0);
            }
        }
    }

}