package SRG3;

public class showHeader {
    public static void showHeader() {
        long startLoad = System.currentTimeMillis();
        Loading loading = new Loading("",300, false);
        Loading loading1 = new Loading("\n" +
                "   _____ _                               _                 _____ ____  \n" +
                "  / ____(_)                             | |               / ____|___ \\ \n" +
                " | (___  _  ___ _ __ ___  _ __ ___  __ _| |__    ______  | |  __  __) |\n",500,false);
        Loading loading2 = new Loading( "  \\___ \\| |/ _ \\ '_ ` _ \\| '__/ _ \\/ _` | '_ \\  |______| | | |_ ||__ < \n" +
                "  ____) | |  __/ | | | | | | |  __/ (_| | |_) |          | |__| |___) |\n",500,false);



        Loading loading3 = new Loading( " |_____/|_|\\___|_| |_| |_|_|  \\___|\\__,_|_.__/            \\_____|____/ \n" +
                "                                                                       \n" +
                "                                                                       \n",500,false);
        try {
            System.out.println("\t\t\t\t\t\t\t Welcome to "+"\n"+"\t\t\t\t\t\t"+"Stoct Management System");
            loading.start();
            loading.join();
            loading1.start();
            loading1.join();
            loading2.start();
            loading2.join();
            loading3.start();
            loading3.join();
        }catch (Exception ex){
            ex.printStackTrace();
        }
        Loading.waiting();
        System.out.println("\n"+"Current Time Loading: "+ (System.currentTimeMillis() - startLoad)/1000);
    }
}
